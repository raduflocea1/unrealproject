// Fill out your copyright notice in the Description page of Project Settings.


#include "QuestCollisionActor.h"
#include "REALunrealcppCharacter.h"
#include "Water.h"

// Sets default values
AQuestCollisionActor::AQuestCollisionActor()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	QMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Quest Mesh"));
	QCollision = CreateDefaultSubobject<USphereComponent>(TEXT("Quest Collision"));
	QRoot = CreateDefaultSubobject<USceneComponent>(TEXT("Quest Root"));

	RootComponent = QRoot;
	QMesh->SetupAttachment(RootComponent);
	QCollision->SetupAttachment(QMesh);
	QCollision->OnComponentBeginOverlap.AddDynamic(this, &AQuestCollisionActor::OnOverlapBegin);

}

// Called when the game starts or when spawned
void AQuestCollisionActor::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AQuestCollisionActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AQuestCollisionActor::OnOverlapBegin(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	AREALunrealcppCharacter* Character = Cast<AREALunrealcppCharacter>(OtherActor);
	if (Character)
	{ 
		if (nr == 1)
		{
			Character->Quest = "Good job!\n Now jump in the water\nto finish the quest!";
			//AWater* finish = Cast<AWater>(OtherActor);
			//finish->
		}
		if (nr == 2) Character->Quest = "Congratulations! You finished the quest!";
	}
}

