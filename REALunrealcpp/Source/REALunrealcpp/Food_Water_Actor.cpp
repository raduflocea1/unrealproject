// Fill out your copyright notice in the Description page of Project Settings.


#include "Food_Water_Actor.h"
#include "REALunrealcppCharacter.h"

// Sets default values
AFood_Water_Actor::AFood_Water_Actor()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	StaticMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Static Mesh"));
	SphereCollision = CreateDefaultSubobject<USphereComponent>(TEXT("Sphere Collision"));
	SceneRoot = CreateDefaultSubobject<USceneComponent>(TEXT("Scene Root"));

	RootComponent = SceneRoot;
	StaticMesh->SetupAttachment(RootComponent);
	SphereCollision->SetupAttachment(StaticMesh);
	SphereCollision->OnComponentBeginOverlap.AddDynamic(this, &AFood_Water_Actor::OnOverlapBegin);

}

// Called when the game starts or when spawned
void AFood_Water_Actor::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AFood_Water_Actor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AFood_Water_Actor::OnOverlapBegin(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	AREALunrealcppCharacter* Character = Cast<AREALunrealcppCharacter>(OtherActor);
	if (Character)
	{
		if (Type == 1)
			if (Character->Food <= Character->MaxFood - 0.2f) Character->Food += 0.2f; else Character->Food = 1.f;
		
		if (Type == 2)
			if (Character->Water <= Character->MaxWater - 0.2f) Character->Water += 0.2f; else Character->Water = 1.f;

		if (Type == 3)
		{
			if (Character->Food <= Character->MaxFood - 0.2f) Character->Food += 0.2f; else Character->Food = 1.f;
			if (Character->Water <= Character->MaxWater - 0.2f) Character->Water += 0.2f; else Character->Water = 1.f;
		}
	}
	Destroy();
}

