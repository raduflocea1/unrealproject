// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "REALunrealcppCharacter.generated.h"

UCLASS(config=Game)
class AREALunrealcppCharacter : public ACharacter
{
	GENERATED_BODY()

	/** Camera boom positioning the camera behind the character */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class USpringArmComponent* CameraBoom;

	/** Follow camera */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class UCameraComponent* FollowCamera;
public:
	AREALunrealcppCharacter();

	/** Base turn rate, in deg/sec. Other scaling may affect final turn rate. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category=Camera)
	float BaseTurnRate;

	/** Base look up/down rate, in deg/sec. Other scaling may affect final rate. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category=Camera)
	float BaseLookUpRate;

protected:

	/** Resets HMD orientation in VR. */
	void OnResetVR();

	/** Called for forwards/backward input */
	void MoveForward(float Value);

	/** Called for side to side input */
	void MoveRight(float Value);

	//Begin play
	virtual void BeginPlay() override;


	/** 
	 * Called via input to turn at a given rate. 
	 * @param Rate	This is a normalized rate, i.e. 1.0 means 100% of desired turn rate
	 */
	void TurnAtRate(float Rate);

	/**
	 * Called via input to turn look up/down at a given rate. 
	 * @param Rate	This is a normalized rate, i.e. 1.0 means 100% of desired turn rate
	 */
	void LookUpAtRate(float Rate);

	/** Handler for when a touch input begins. */
	void TouchStarted(ETouchIndex::Type FingerIndex, FVector Location);

	/** Handler for when a touch input stops. */
	void TouchStopped(ETouchIndex::Type FingerIndex, FVector Location);

protected:
	// APawn interface
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;
	// End of APawn interface
	///////////////////////////////

	virtual void Landed(const FHitResult& Hit) override;

public:
	/** Returns CameraBoom subobject **/
	FORCEINLINE class USpringArmComponent* GetCameraBoom() const { return CameraBoom; }
	/** Returns FollowCamera subobject **/
	FORCEINLINE class UCameraComponent* GetFollowCamera() const { return FollowCamera; }

	///////////////////////////////////////////////////////////////////////////////////////////

	UFUNCTION()
		void DoubleJump();

	UPROPERTY()
		int DoubleJumpCounter;

	UPROPERTY(EditAnywhere,BlueprintReadWrite)
		float JumpHeight;

	UFUNCTION()
		void Sprint();

	UFUNCTION()
		void Walk();

	UPROPERTY(EditAnywhere)
		float MaxSpeed;

	UPROPERTY(EditAnywhere)
		float WalkSpeed;

	UFUNCTION()
		void Dash();

	UPROPERTY(EditAnywhere)
		float DashDistance;

	UPROPERTY(EditAnywhere)
		float DashCooldown;

	UPROPERTY(EditAnywhere)
		float CanDash;

	UPROPERTY(EditAnywhere)
		float DashStop;
	
	UPROPERTY(EditAnywhere)
		FTimerHandle UnusedHandle;

	UFUNCTION()
		void StopDashing();

	UFUNCTION()
		void ResetDash();

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		float Water;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		float Food;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		float MaxWater;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		float MaxFood;

	UPROPERTY()
		FTimerHandle Unused;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		float FoodWaterDrainRate;

	UFUNCTION(BlueprintCallable)
		void DrainFoodWater();
	
	UFUNCTION(BlueprintCallable)
		void Teleport();

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		FString Quest;




	 

};

