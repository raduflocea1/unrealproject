// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef REALUNREALCPP_REALunrealcppCharacter_generated_h
#error "REALunrealcppCharacter.generated.h already included, missing '#pragma once' in REALunrealcppCharacter.h"
#endif
#define REALUNREALCPP_REALunrealcppCharacter_generated_h

#define REALunrealcpp_Source_REALunrealcpp_REALunrealcppCharacter_h_12_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execTeleport) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->Teleport(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execDrainFoodWater) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->DrainFoodWater(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execResetDash) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->ResetDash(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execStopDashing) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->StopDashing(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execDash) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->Dash(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execWalk) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->Walk(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execSprint) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->Sprint(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execDoubleJump) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->DoubleJump(); \
		P_NATIVE_END; \
	}


#define REALunrealcpp_Source_REALunrealcpp_REALunrealcppCharacter_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execTeleport) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->Teleport(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execDrainFoodWater) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->DrainFoodWater(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execResetDash) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->ResetDash(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execStopDashing) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->StopDashing(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execDash) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->Dash(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execWalk) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->Walk(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execSprint) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->Sprint(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execDoubleJump) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->DoubleJump(); \
		P_NATIVE_END; \
	}


#define REALunrealcpp_Source_REALunrealcpp_REALunrealcppCharacter_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAREALunrealcppCharacter(); \
	friend struct Z_Construct_UClass_AREALunrealcppCharacter_Statics; \
public: \
	DECLARE_CLASS(AREALunrealcppCharacter, ACharacter, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/REALunrealcpp"), NO_API) \
	DECLARE_SERIALIZER(AREALunrealcppCharacter)


#define REALunrealcpp_Source_REALunrealcpp_REALunrealcppCharacter_h_12_INCLASS \
private: \
	static void StaticRegisterNativesAREALunrealcppCharacter(); \
	friend struct Z_Construct_UClass_AREALunrealcppCharacter_Statics; \
public: \
	DECLARE_CLASS(AREALunrealcppCharacter, ACharacter, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/REALunrealcpp"), NO_API) \
	DECLARE_SERIALIZER(AREALunrealcppCharacter)


#define REALunrealcpp_Source_REALunrealcpp_REALunrealcppCharacter_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AREALunrealcppCharacter(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AREALunrealcppCharacter) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AREALunrealcppCharacter); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AREALunrealcppCharacter); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AREALunrealcppCharacter(AREALunrealcppCharacter&&); \
	NO_API AREALunrealcppCharacter(const AREALunrealcppCharacter&); \
public:


#define REALunrealcpp_Source_REALunrealcpp_REALunrealcppCharacter_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AREALunrealcppCharacter(AREALunrealcppCharacter&&); \
	NO_API AREALunrealcppCharacter(const AREALunrealcppCharacter&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AREALunrealcppCharacter); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AREALunrealcppCharacter); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AREALunrealcppCharacter)


#define REALunrealcpp_Source_REALunrealcpp_REALunrealcppCharacter_h_12_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__CameraBoom() { return STRUCT_OFFSET(AREALunrealcppCharacter, CameraBoom); } \
	FORCEINLINE static uint32 __PPO__FollowCamera() { return STRUCT_OFFSET(AREALunrealcppCharacter, FollowCamera); }


#define REALunrealcpp_Source_REALunrealcpp_REALunrealcppCharacter_h_9_PROLOG
#define REALunrealcpp_Source_REALunrealcpp_REALunrealcppCharacter_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	REALunrealcpp_Source_REALunrealcpp_REALunrealcppCharacter_h_12_PRIVATE_PROPERTY_OFFSET \
	REALunrealcpp_Source_REALunrealcpp_REALunrealcppCharacter_h_12_RPC_WRAPPERS \
	REALunrealcpp_Source_REALunrealcpp_REALunrealcppCharacter_h_12_INCLASS \
	REALunrealcpp_Source_REALunrealcpp_REALunrealcppCharacter_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define REALunrealcpp_Source_REALunrealcpp_REALunrealcppCharacter_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	REALunrealcpp_Source_REALunrealcpp_REALunrealcppCharacter_h_12_PRIVATE_PROPERTY_OFFSET \
	REALunrealcpp_Source_REALunrealcpp_REALunrealcppCharacter_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	REALunrealcpp_Source_REALunrealcpp_REALunrealcppCharacter_h_12_INCLASS_NO_PURE_DECLS \
	REALunrealcpp_Source_REALunrealcpp_REALunrealcppCharacter_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> REALUNREALCPP_API UClass* StaticClass<class AREALunrealcppCharacter>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID REALunrealcpp_Source_REALunrealcpp_REALunrealcppCharacter_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
