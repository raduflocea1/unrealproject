// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class UPrimitiveComponent;
class AActor;
struct FHitResult;
#ifdef REALUNREALCPP_Food_Water_Actor_generated_h
#error "Food_Water_Actor.generated.h already included, missing '#pragma once' in Food_Water_Actor.h"
#endif
#define REALUNREALCPP_Food_Water_Actor_generated_h

#define REALunrealcpp_Source_REALunrealcpp_Food_Water_Actor_h_15_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execOnOverlapBegin) \
	{ \
		P_GET_OBJECT(UPrimitiveComponent,Z_Param_OverlappedComp); \
		P_GET_OBJECT(AActor,Z_Param_OtherActor); \
		P_GET_OBJECT(UPrimitiveComponent,Z_Param_OtherComp); \
		P_GET_PROPERTY(UIntProperty,Z_Param_OtherBodyIndex); \
		P_GET_UBOOL(Z_Param_bFromSweep); \
		P_GET_STRUCT_REF(FHitResult,Z_Param_Out_SweepResult); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->OnOverlapBegin(Z_Param_OverlappedComp,Z_Param_OtherActor,Z_Param_OtherComp,Z_Param_OtherBodyIndex,Z_Param_bFromSweep,Z_Param_Out_SweepResult); \
		P_NATIVE_END; \
	}


#define REALunrealcpp_Source_REALunrealcpp_Food_Water_Actor_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execOnOverlapBegin) \
	{ \
		P_GET_OBJECT(UPrimitiveComponent,Z_Param_OverlappedComp); \
		P_GET_OBJECT(AActor,Z_Param_OtherActor); \
		P_GET_OBJECT(UPrimitiveComponent,Z_Param_OtherComp); \
		P_GET_PROPERTY(UIntProperty,Z_Param_OtherBodyIndex); \
		P_GET_UBOOL(Z_Param_bFromSweep); \
		P_GET_STRUCT_REF(FHitResult,Z_Param_Out_SweepResult); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->OnOverlapBegin(Z_Param_OverlappedComp,Z_Param_OtherActor,Z_Param_OtherComp,Z_Param_OtherBodyIndex,Z_Param_bFromSweep,Z_Param_Out_SweepResult); \
		P_NATIVE_END; \
	}


#define REALunrealcpp_Source_REALunrealcpp_Food_Water_Actor_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAFood_Water_Actor(); \
	friend struct Z_Construct_UClass_AFood_Water_Actor_Statics; \
public: \
	DECLARE_CLASS(AFood_Water_Actor, AActor, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/REALunrealcpp"), NO_API) \
	DECLARE_SERIALIZER(AFood_Water_Actor)


#define REALunrealcpp_Source_REALunrealcpp_Food_Water_Actor_h_15_INCLASS \
private: \
	static void StaticRegisterNativesAFood_Water_Actor(); \
	friend struct Z_Construct_UClass_AFood_Water_Actor_Statics; \
public: \
	DECLARE_CLASS(AFood_Water_Actor, AActor, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/REALunrealcpp"), NO_API) \
	DECLARE_SERIALIZER(AFood_Water_Actor)


#define REALunrealcpp_Source_REALunrealcpp_Food_Water_Actor_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AFood_Water_Actor(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AFood_Water_Actor) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AFood_Water_Actor); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AFood_Water_Actor); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AFood_Water_Actor(AFood_Water_Actor&&); \
	NO_API AFood_Water_Actor(const AFood_Water_Actor&); \
public:


#define REALunrealcpp_Source_REALunrealcpp_Food_Water_Actor_h_15_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AFood_Water_Actor(AFood_Water_Actor&&); \
	NO_API AFood_Water_Actor(const AFood_Water_Actor&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AFood_Water_Actor); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AFood_Water_Actor); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AFood_Water_Actor)


#define REALunrealcpp_Source_REALunrealcpp_Food_Water_Actor_h_15_PRIVATE_PROPERTY_OFFSET
#define REALunrealcpp_Source_REALunrealcpp_Food_Water_Actor_h_12_PROLOG
#define REALunrealcpp_Source_REALunrealcpp_Food_Water_Actor_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	REALunrealcpp_Source_REALunrealcpp_Food_Water_Actor_h_15_PRIVATE_PROPERTY_OFFSET \
	REALunrealcpp_Source_REALunrealcpp_Food_Water_Actor_h_15_RPC_WRAPPERS \
	REALunrealcpp_Source_REALunrealcpp_Food_Water_Actor_h_15_INCLASS \
	REALunrealcpp_Source_REALunrealcpp_Food_Water_Actor_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define REALunrealcpp_Source_REALunrealcpp_Food_Water_Actor_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	REALunrealcpp_Source_REALunrealcpp_Food_Water_Actor_h_15_PRIVATE_PROPERTY_OFFSET \
	REALunrealcpp_Source_REALunrealcpp_Food_Water_Actor_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	REALunrealcpp_Source_REALunrealcpp_Food_Water_Actor_h_15_INCLASS_NO_PURE_DECLS \
	REALunrealcpp_Source_REALunrealcpp_Food_Water_Actor_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> REALUNREALCPP_API UClass* StaticClass<class AFood_Water_Actor>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID REALunrealcpp_Source_REALunrealcpp_Food_Water_Actor_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
