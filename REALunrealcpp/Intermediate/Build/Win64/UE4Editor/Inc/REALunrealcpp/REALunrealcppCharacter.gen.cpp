// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "REALunrealcpp/REALunrealcppCharacter.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeREALunrealcppCharacter() {}
// Cross Module References
	REALUNREALCPP_API UClass* Z_Construct_UClass_AREALunrealcppCharacter_NoRegister();
	REALUNREALCPP_API UClass* Z_Construct_UClass_AREALunrealcppCharacter();
	ENGINE_API UClass* Z_Construct_UClass_ACharacter();
	UPackage* Z_Construct_UPackage__Script_REALunrealcpp();
	REALUNREALCPP_API UFunction* Z_Construct_UFunction_AREALunrealcppCharacter_Dash();
	REALUNREALCPP_API UFunction* Z_Construct_UFunction_AREALunrealcppCharacter_DoubleJump();
	REALUNREALCPP_API UFunction* Z_Construct_UFunction_AREALunrealcppCharacter_DrainFoodWater();
	REALUNREALCPP_API UFunction* Z_Construct_UFunction_AREALunrealcppCharacter_ResetDash();
	REALUNREALCPP_API UFunction* Z_Construct_UFunction_AREALunrealcppCharacter_Sprint();
	REALUNREALCPP_API UFunction* Z_Construct_UFunction_AREALunrealcppCharacter_StopDashing();
	REALUNREALCPP_API UFunction* Z_Construct_UFunction_AREALunrealcppCharacter_Teleport();
	REALUNREALCPP_API UFunction* Z_Construct_UFunction_AREALunrealcppCharacter_Walk();
	ENGINE_API UScriptStruct* Z_Construct_UScriptStruct_FTimerHandle();
	ENGINE_API UClass* Z_Construct_UClass_UCameraComponent_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_USpringArmComponent_NoRegister();
// End Cross Module References
	void AREALunrealcppCharacter::StaticRegisterNativesAREALunrealcppCharacter()
	{
		UClass* Class = AREALunrealcppCharacter::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "Dash", &AREALunrealcppCharacter::execDash },
			{ "DoubleJump", &AREALunrealcppCharacter::execDoubleJump },
			{ "DrainFoodWater", &AREALunrealcppCharacter::execDrainFoodWater },
			{ "ResetDash", &AREALunrealcppCharacter::execResetDash },
			{ "Sprint", &AREALunrealcppCharacter::execSprint },
			{ "StopDashing", &AREALunrealcppCharacter::execStopDashing },
			{ "Teleport", &AREALunrealcppCharacter::execTeleport },
			{ "Walk", &AREALunrealcppCharacter::execWalk },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_AREALunrealcppCharacter_Dash_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AREALunrealcppCharacter_Dash_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "REALunrealcppCharacter.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AREALunrealcppCharacter_Dash_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AREALunrealcppCharacter, nullptr, "Dash", 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AREALunrealcppCharacter_Dash_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_AREALunrealcppCharacter_Dash_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AREALunrealcppCharacter_Dash()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AREALunrealcppCharacter_Dash_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_AREALunrealcppCharacter_DoubleJump_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AREALunrealcppCharacter_DoubleJump_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "REALunrealcppCharacter.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AREALunrealcppCharacter_DoubleJump_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AREALunrealcppCharacter, nullptr, "DoubleJump", 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AREALunrealcppCharacter_DoubleJump_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_AREALunrealcppCharacter_DoubleJump_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AREALunrealcppCharacter_DoubleJump()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AREALunrealcppCharacter_DoubleJump_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_AREALunrealcppCharacter_DrainFoodWater_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AREALunrealcppCharacter_DrainFoodWater_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "REALunrealcppCharacter.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AREALunrealcppCharacter_DrainFoodWater_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AREALunrealcppCharacter, nullptr, "DrainFoodWater", 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AREALunrealcppCharacter_DrainFoodWater_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_AREALunrealcppCharacter_DrainFoodWater_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AREALunrealcppCharacter_DrainFoodWater()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AREALunrealcppCharacter_DrainFoodWater_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_AREALunrealcppCharacter_ResetDash_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AREALunrealcppCharacter_ResetDash_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "REALunrealcppCharacter.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AREALunrealcppCharacter_ResetDash_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AREALunrealcppCharacter, nullptr, "ResetDash", 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AREALunrealcppCharacter_ResetDash_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_AREALunrealcppCharacter_ResetDash_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AREALunrealcppCharacter_ResetDash()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AREALunrealcppCharacter_ResetDash_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_AREALunrealcppCharacter_Sprint_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AREALunrealcppCharacter_Sprint_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "REALunrealcppCharacter.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AREALunrealcppCharacter_Sprint_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AREALunrealcppCharacter, nullptr, "Sprint", 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AREALunrealcppCharacter_Sprint_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_AREALunrealcppCharacter_Sprint_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AREALunrealcppCharacter_Sprint()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AREALunrealcppCharacter_Sprint_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_AREALunrealcppCharacter_StopDashing_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AREALunrealcppCharacter_StopDashing_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "REALunrealcppCharacter.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AREALunrealcppCharacter_StopDashing_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AREALunrealcppCharacter, nullptr, "StopDashing", 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AREALunrealcppCharacter_StopDashing_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_AREALunrealcppCharacter_StopDashing_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AREALunrealcppCharacter_StopDashing()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AREALunrealcppCharacter_StopDashing_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_AREALunrealcppCharacter_Teleport_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AREALunrealcppCharacter_Teleport_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "REALunrealcppCharacter.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AREALunrealcppCharacter_Teleport_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AREALunrealcppCharacter, nullptr, "Teleport", 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AREALunrealcppCharacter_Teleport_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_AREALunrealcppCharacter_Teleport_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AREALunrealcppCharacter_Teleport()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AREALunrealcppCharacter_Teleport_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_AREALunrealcppCharacter_Walk_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AREALunrealcppCharacter_Walk_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "REALunrealcppCharacter.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AREALunrealcppCharacter_Walk_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AREALunrealcppCharacter, nullptr, "Walk", 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AREALunrealcppCharacter_Walk_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_AREALunrealcppCharacter_Walk_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AREALunrealcppCharacter_Walk()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AREALunrealcppCharacter_Walk_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_AREALunrealcppCharacter_NoRegister()
	{
		return AREALunrealcppCharacter::StaticClass();
	}
	struct Z_Construct_UClass_AREALunrealcppCharacter_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Quest_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_Quest;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FoodWaterDrainRate_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_FoodWaterDrainRate;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Unused_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Unused;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MaxFood_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_MaxFood;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MaxWater_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_MaxWater;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Food_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Food;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Water_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Water;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_UnusedHandle_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_UnusedHandle;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DashStop_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_DashStop;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CanDash_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_CanDash;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DashCooldown_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_DashCooldown;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DashDistance_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_DashDistance;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_WalkSpeed_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_WalkSpeed;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MaxSpeed_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_MaxSpeed;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_JumpHeight_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_JumpHeight;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DoubleJumpCounter_MetaData[];
#endif
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_DoubleJumpCounter;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_BaseLookUpRate_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_BaseLookUpRate;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_BaseTurnRate_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_BaseTurnRate;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FollowCamera_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_FollowCamera;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CameraBoom_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_CameraBoom;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_AREALunrealcppCharacter_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_ACharacter,
		(UObject* (*)())Z_Construct_UPackage__Script_REALunrealcpp,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_AREALunrealcppCharacter_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_AREALunrealcppCharacter_Dash, "Dash" }, // 4219725729
		{ &Z_Construct_UFunction_AREALunrealcppCharacter_DoubleJump, "DoubleJump" }, // 1682887046
		{ &Z_Construct_UFunction_AREALunrealcppCharacter_DrainFoodWater, "DrainFoodWater" }, // 695915100
		{ &Z_Construct_UFunction_AREALunrealcppCharacter_ResetDash, "ResetDash" }, // 2350691369
		{ &Z_Construct_UFunction_AREALunrealcppCharacter_Sprint, "Sprint" }, // 12413914
		{ &Z_Construct_UFunction_AREALunrealcppCharacter_StopDashing, "StopDashing" }, // 3657738767
		{ &Z_Construct_UFunction_AREALunrealcppCharacter_Teleport, "Teleport" }, // 1137812107
		{ &Z_Construct_UFunction_AREALunrealcppCharacter_Walk, "Walk" }, // 4078283596
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AREALunrealcppCharacter_Statics::Class_MetaDataParams[] = {
		{ "HideCategories", "Navigation" },
		{ "IncludePath", "REALunrealcppCharacter.h" },
		{ "ModuleRelativePath", "REALunrealcppCharacter.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AREALunrealcppCharacter_Statics::NewProp_Quest_MetaData[] = {
		{ "Category", "REALunrealcppCharacter" },
		{ "ModuleRelativePath", "REALunrealcppCharacter.h" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_AREALunrealcppCharacter_Statics::NewProp_Quest = { "Quest", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AREALunrealcppCharacter, Quest), METADATA_PARAMS(Z_Construct_UClass_AREALunrealcppCharacter_Statics::NewProp_Quest_MetaData, ARRAY_COUNT(Z_Construct_UClass_AREALunrealcppCharacter_Statics::NewProp_Quest_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AREALunrealcppCharacter_Statics::NewProp_FoodWaterDrainRate_MetaData[] = {
		{ "Category", "REALunrealcppCharacter" },
		{ "ModuleRelativePath", "REALunrealcppCharacter.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_AREALunrealcppCharacter_Statics::NewProp_FoodWaterDrainRate = { "FoodWaterDrainRate", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AREALunrealcppCharacter, FoodWaterDrainRate), METADATA_PARAMS(Z_Construct_UClass_AREALunrealcppCharacter_Statics::NewProp_FoodWaterDrainRate_MetaData, ARRAY_COUNT(Z_Construct_UClass_AREALunrealcppCharacter_Statics::NewProp_FoodWaterDrainRate_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AREALunrealcppCharacter_Statics::NewProp_Unused_MetaData[] = {
		{ "ModuleRelativePath", "REALunrealcppCharacter.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_AREALunrealcppCharacter_Statics::NewProp_Unused = { "Unused", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AREALunrealcppCharacter, Unused), Z_Construct_UScriptStruct_FTimerHandle, METADATA_PARAMS(Z_Construct_UClass_AREALunrealcppCharacter_Statics::NewProp_Unused_MetaData, ARRAY_COUNT(Z_Construct_UClass_AREALunrealcppCharacter_Statics::NewProp_Unused_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AREALunrealcppCharacter_Statics::NewProp_MaxFood_MetaData[] = {
		{ "Category", "REALunrealcppCharacter" },
		{ "ModuleRelativePath", "REALunrealcppCharacter.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_AREALunrealcppCharacter_Statics::NewProp_MaxFood = { "MaxFood", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AREALunrealcppCharacter, MaxFood), METADATA_PARAMS(Z_Construct_UClass_AREALunrealcppCharacter_Statics::NewProp_MaxFood_MetaData, ARRAY_COUNT(Z_Construct_UClass_AREALunrealcppCharacter_Statics::NewProp_MaxFood_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AREALunrealcppCharacter_Statics::NewProp_MaxWater_MetaData[] = {
		{ "Category", "REALunrealcppCharacter" },
		{ "ModuleRelativePath", "REALunrealcppCharacter.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_AREALunrealcppCharacter_Statics::NewProp_MaxWater = { "MaxWater", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AREALunrealcppCharacter, MaxWater), METADATA_PARAMS(Z_Construct_UClass_AREALunrealcppCharacter_Statics::NewProp_MaxWater_MetaData, ARRAY_COUNT(Z_Construct_UClass_AREALunrealcppCharacter_Statics::NewProp_MaxWater_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AREALunrealcppCharacter_Statics::NewProp_Food_MetaData[] = {
		{ "Category", "REALunrealcppCharacter" },
		{ "ModuleRelativePath", "REALunrealcppCharacter.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_AREALunrealcppCharacter_Statics::NewProp_Food = { "Food", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AREALunrealcppCharacter, Food), METADATA_PARAMS(Z_Construct_UClass_AREALunrealcppCharacter_Statics::NewProp_Food_MetaData, ARRAY_COUNT(Z_Construct_UClass_AREALunrealcppCharacter_Statics::NewProp_Food_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AREALunrealcppCharacter_Statics::NewProp_Water_MetaData[] = {
		{ "Category", "REALunrealcppCharacter" },
		{ "ModuleRelativePath", "REALunrealcppCharacter.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_AREALunrealcppCharacter_Statics::NewProp_Water = { "Water", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AREALunrealcppCharacter, Water), METADATA_PARAMS(Z_Construct_UClass_AREALunrealcppCharacter_Statics::NewProp_Water_MetaData, ARRAY_COUNT(Z_Construct_UClass_AREALunrealcppCharacter_Statics::NewProp_Water_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AREALunrealcppCharacter_Statics::NewProp_UnusedHandle_MetaData[] = {
		{ "Category", "REALunrealcppCharacter" },
		{ "ModuleRelativePath", "REALunrealcppCharacter.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_AREALunrealcppCharacter_Statics::NewProp_UnusedHandle = { "UnusedHandle", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AREALunrealcppCharacter, UnusedHandle), Z_Construct_UScriptStruct_FTimerHandle, METADATA_PARAMS(Z_Construct_UClass_AREALunrealcppCharacter_Statics::NewProp_UnusedHandle_MetaData, ARRAY_COUNT(Z_Construct_UClass_AREALunrealcppCharacter_Statics::NewProp_UnusedHandle_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AREALunrealcppCharacter_Statics::NewProp_DashStop_MetaData[] = {
		{ "Category", "REALunrealcppCharacter" },
		{ "ModuleRelativePath", "REALunrealcppCharacter.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_AREALunrealcppCharacter_Statics::NewProp_DashStop = { "DashStop", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AREALunrealcppCharacter, DashStop), METADATA_PARAMS(Z_Construct_UClass_AREALunrealcppCharacter_Statics::NewProp_DashStop_MetaData, ARRAY_COUNT(Z_Construct_UClass_AREALunrealcppCharacter_Statics::NewProp_DashStop_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AREALunrealcppCharacter_Statics::NewProp_CanDash_MetaData[] = {
		{ "Category", "REALunrealcppCharacter" },
		{ "ModuleRelativePath", "REALunrealcppCharacter.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_AREALunrealcppCharacter_Statics::NewProp_CanDash = { "CanDash", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AREALunrealcppCharacter, CanDash), METADATA_PARAMS(Z_Construct_UClass_AREALunrealcppCharacter_Statics::NewProp_CanDash_MetaData, ARRAY_COUNT(Z_Construct_UClass_AREALunrealcppCharacter_Statics::NewProp_CanDash_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AREALunrealcppCharacter_Statics::NewProp_DashCooldown_MetaData[] = {
		{ "Category", "REALunrealcppCharacter" },
		{ "ModuleRelativePath", "REALunrealcppCharacter.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_AREALunrealcppCharacter_Statics::NewProp_DashCooldown = { "DashCooldown", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AREALunrealcppCharacter, DashCooldown), METADATA_PARAMS(Z_Construct_UClass_AREALunrealcppCharacter_Statics::NewProp_DashCooldown_MetaData, ARRAY_COUNT(Z_Construct_UClass_AREALunrealcppCharacter_Statics::NewProp_DashCooldown_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AREALunrealcppCharacter_Statics::NewProp_DashDistance_MetaData[] = {
		{ "Category", "REALunrealcppCharacter" },
		{ "ModuleRelativePath", "REALunrealcppCharacter.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_AREALunrealcppCharacter_Statics::NewProp_DashDistance = { "DashDistance", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AREALunrealcppCharacter, DashDistance), METADATA_PARAMS(Z_Construct_UClass_AREALunrealcppCharacter_Statics::NewProp_DashDistance_MetaData, ARRAY_COUNT(Z_Construct_UClass_AREALunrealcppCharacter_Statics::NewProp_DashDistance_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AREALunrealcppCharacter_Statics::NewProp_WalkSpeed_MetaData[] = {
		{ "Category", "REALunrealcppCharacter" },
		{ "ModuleRelativePath", "REALunrealcppCharacter.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_AREALunrealcppCharacter_Statics::NewProp_WalkSpeed = { "WalkSpeed", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AREALunrealcppCharacter, WalkSpeed), METADATA_PARAMS(Z_Construct_UClass_AREALunrealcppCharacter_Statics::NewProp_WalkSpeed_MetaData, ARRAY_COUNT(Z_Construct_UClass_AREALunrealcppCharacter_Statics::NewProp_WalkSpeed_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AREALunrealcppCharacter_Statics::NewProp_MaxSpeed_MetaData[] = {
		{ "Category", "REALunrealcppCharacter" },
		{ "ModuleRelativePath", "REALunrealcppCharacter.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_AREALunrealcppCharacter_Statics::NewProp_MaxSpeed = { "MaxSpeed", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AREALunrealcppCharacter, MaxSpeed), METADATA_PARAMS(Z_Construct_UClass_AREALunrealcppCharacter_Statics::NewProp_MaxSpeed_MetaData, ARRAY_COUNT(Z_Construct_UClass_AREALunrealcppCharacter_Statics::NewProp_MaxSpeed_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AREALunrealcppCharacter_Statics::NewProp_JumpHeight_MetaData[] = {
		{ "Category", "REALunrealcppCharacter" },
		{ "ModuleRelativePath", "REALunrealcppCharacter.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_AREALunrealcppCharacter_Statics::NewProp_JumpHeight = { "JumpHeight", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AREALunrealcppCharacter, JumpHeight), METADATA_PARAMS(Z_Construct_UClass_AREALunrealcppCharacter_Statics::NewProp_JumpHeight_MetaData, ARRAY_COUNT(Z_Construct_UClass_AREALunrealcppCharacter_Statics::NewProp_JumpHeight_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AREALunrealcppCharacter_Statics::NewProp_DoubleJumpCounter_MetaData[] = {
		{ "ModuleRelativePath", "REALunrealcppCharacter.h" },
	};
#endif
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UClass_AREALunrealcppCharacter_Statics::NewProp_DoubleJumpCounter = { "DoubleJumpCounter", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AREALunrealcppCharacter, DoubleJumpCounter), METADATA_PARAMS(Z_Construct_UClass_AREALunrealcppCharacter_Statics::NewProp_DoubleJumpCounter_MetaData, ARRAY_COUNT(Z_Construct_UClass_AREALunrealcppCharacter_Statics::NewProp_DoubleJumpCounter_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AREALunrealcppCharacter_Statics::NewProp_BaseLookUpRate_MetaData[] = {
		{ "Category", "Camera" },
		{ "ModuleRelativePath", "REALunrealcppCharacter.h" },
		{ "ToolTip", "Base look up/down rate, in deg/sec. Other scaling may affect final rate." },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_AREALunrealcppCharacter_Statics::NewProp_BaseLookUpRate = { "BaseLookUpRate", nullptr, (EPropertyFlags)0x0010000000020015, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AREALunrealcppCharacter, BaseLookUpRate), METADATA_PARAMS(Z_Construct_UClass_AREALunrealcppCharacter_Statics::NewProp_BaseLookUpRate_MetaData, ARRAY_COUNT(Z_Construct_UClass_AREALunrealcppCharacter_Statics::NewProp_BaseLookUpRate_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AREALunrealcppCharacter_Statics::NewProp_BaseTurnRate_MetaData[] = {
		{ "Category", "Camera" },
		{ "ModuleRelativePath", "REALunrealcppCharacter.h" },
		{ "ToolTip", "Base turn rate, in deg/sec. Other scaling may affect final turn rate." },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_AREALunrealcppCharacter_Statics::NewProp_BaseTurnRate = { "BaseTurnRate", nullptr, (EPropertyFlags)0x0010000000020015, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AREALunrealcppCharacter, BaseTurnRate), METADATA_PARAMS(Z_Construct_UClass_AREALunrealcppCharacter_Statics::NewProp_BaseTurnRate_MetaData, ARRAY_COUNT(Z_Construct_UClass_AREALunrealcppCharacter_Statics::NewProp_BaseTurnRate_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AREALunrealcppCharacter_Statics::NewProp_FollowCamera_MetaData[] = {
		{ "AllowPrivateAccess", "true" },
		{ "Category", "Camera" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "REALunrealcppCharacter.h" },
		{ "ToolTip", "Follow camera" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AREALunrealcppCharacter_Statics::NewProp_FollowCamera = { "FollowCamera", nullptr, (EPropertyFlags)0x00400000000a001d, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AREALunrealcppCharacter, FollowCamera), Z_Construct_UClass_UCameraComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_AREALunrealcppCharacter_Statics::NewProp_FollowCamera_MetaData, ARRAY_COUNT(Z_Construct_UClass_AREALunrealcppCharacter_Statics::NewProp_FollowCamera_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AREALunrealcppCharacter_Statics::NewProp_CameraBoom_MetaData[] = {
		{ "AllowPrivateAccess", "true" },
		{ "Category", "Camera" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "REALunrealcppCharacter.h" },
		{ "ToolTip", "Camera boom positioning the camera behind the character" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AREALunrealcppCharacter_Statics::NewProp_CameraBoom = { "CameraBoom", nullptr, (EPropertyFlags)0x00400000000a001d, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AREALunrealcppCharacter, CameraBoom), Z_Construct_UClass_USpringArmComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_AREALunrealcppCharacter_Statics::NewProp_CameraBoom_MetaData, ARRAY_COUNT(Z_Construct_UClass_AREALunrealcppCharacter_Statics::NewProp_CameraBoom_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_AREALunrealcppCharacter_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AREALunrealcppCharacter_Statics::NewProp_Quest,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AREALunrealcppCharacter_Statics::NewProp_FoodWaterDrainRate,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AREALunrealcppCharacter_Statics::NewProp_Unused,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AREALunrealcppCharacter_Statics::NewProp_MaxFood,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AREALunrealcppCharacter_Statics::NewProp_MaxWater,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AREALunrealcppCharacter_Statics::NewProp_Food,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AREALunrealcppCharacter_Statics::NewProp_Water,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AREALunrealcppCharacter_Statics::NewProp_UnusedHandle,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AREALunrealcppCharacter_Statics::NewProp_DashStop,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AREALunrealcppCharacter_Statics::NewProp_CanDash,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AREALunrealcppCharacter_Statics::NewProp_DashCooldown,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AREALunrealcppCharacter_Statics::NewProp_DashDistance,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AREALunrealcppCharacter_Statics::NewProp_WalkSpeed,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AREALunrealcppCharacter_Statics::NewProp_MaxSpeed,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AREALunrealcppCharacter_Statics::NewProp_JumpHeight,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AREALunrealcppCharacter_Statics::NewProp_DoubleJumpCounter,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AREALunrealcppCharacter_Statics::NewProp_BaseLookUpRate,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AREALunrealcppCharacter_Statics::NewProp_BaseTurnRate,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AREALunrealcppCharacter_Statics::NewProp_FollowCamera,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AREALunrealcppCharacter_Statics::NewProp_CameraBoom,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_AREALunrealcppCharacter_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<AREALunrealcppCharacter>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_AREALunrealcppCharacter_Statics::ClassParams = {
		&AREALunrealcppCharacter::StaticClass,
		"Game",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_AREALunrealcppCharacter_Statics::PropPointers,
		nullptr,
		ARRAY_COUNT(DependentSingletons),
		ARRAY_COUNT(FuncInfo),
		ARRAY_COUNT(Z_Construct_UClass_AREALunrealcppCharacter_Statics::PropPointers),
		0,
		0x008000A0u,
		METADATA_PARAMS(Z_Construct_UClass_AREALunrealcppCharacter_Statics::Class_MetaDataParams, ARRAY_COUNT(Z_Construct_UClass_AREALunrealcppCharacter_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_AREALunrealcppCharacter()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_AREALunrealcppCharacter_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(AREALunrealcppCharacter, 624906369);
	template<> REALUNREALCPP_API UClass* StaticClass<AREALunrealcppCharacter>()
	{
		return AREALunrealcppCharacter::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_AREALunrealcppCharacter(Z_Construct_UClass_AREALunrealcppCharacter, &AREALunrealcppCharacter::StaticClass, TEXT("/Script/REALunrealcpp"), TEXT("AREALunrealcppCharacter"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(AREALunrealcppCharacter);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
