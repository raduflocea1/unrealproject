// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "REALunrealcpp/REALunrealcppGameMode.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeREALunrealcppGameMode() {}
// Cross Module References
	REALUNREALCPP_API UClass* Z_Construct_UClass_AREALunrealcppGameMode_NoRegister();
	REALUNREALCPP_API UClass* Z_Construct_UClass_AREALunrealcppGameMode();
	ENGINE_API UClass* Z_Construct_UClass_AGameModeBase();
	UPackage* Z_Construct_UPackage__Script_REALunrealcpp();
// End Cross Module References
	void AREALunrealcppGameMode::StaticRegisterNativesAREALunrealcppGameMode()
	{
	}
	UClass* Z_Construct_UClass_AREALunrealcppGameMode_NoRegister()
	{
		return AREALunrealcppGameMode::StaticClass();
	}
	struct Z_Construct_UClass_AREALunrealcppGameMode_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_AREALunrealcppGameMode_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AGameModeBase,
		(UObject* (*)())Z_Construct_UPackage__Script_REALunrealcpp,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AREALunrealcppGameMode_Statics::Class_MetaDataParams[] = {
		{ "HideCategories", "Info Rendering MovementReplication Replication Actor Input Movement Collision Rendering Utilities|Transformation" },
		{ "IncludePath", "REALunrealcppGameMode.h" },
		{ "ModuleRelativePath", "REALunrealcppGameMode.h" },
		{ "ShowCategories", "Input|MouseInput Input|TouchInput" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_AREALunrealcppGameMode_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<AREALunrealcppGameMode>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_AREALunrealcppGameMode_Statics::ClassParams = {
		&AREALunrealcppGameMode::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x008802A8u,
		METADATA_PARAMS(Z_Construct_UClass_AREALunrealcppGameMode_Statics::Class_MetaDataParams, ARRAY_COUNT(Z_Construct_UClass_AREALunrealcppGameMode_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_AREALunrealcppGameMode()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_AREALunrealcppGameMode_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(AREALunrealcppGameMode, 1137497508);
	template<> REALUNREALCPP_API UClass* StaticClass<AREALunrealcppGameMode>()
	{
		return AREALunrealcppGameMode::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_AREALunrealcppGameMode(Z_Construct_UClass_AREALunrealcppGameMode, &AREALunrealcppGameMode::StaticClass, TEXT("/Script/REALunrealcpp"), TEXT("AREALunrealcppGameMode"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(AREALunrealcppGameMode);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
