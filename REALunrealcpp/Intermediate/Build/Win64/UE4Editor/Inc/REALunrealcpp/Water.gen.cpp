// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "REALunrealcpp/Water.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeWater() {}
// Cross Module References
	REALUNREALCPP_API UClass* Z_Construct_UClass_AWater_NoRegister();
	REALUNREALCPP_API UClass* Z_Construct_UClass_AWater();
	ENGINE_API UClass* Z_Construct_UClass_AActor();
	UPackage* Z_Construct_UPackage__Script_REALunrealcpp();
// End Cross Module References
	void AWater::StaticRegisterNativesAWater()
	{
	}
	UClass* Z_Construct_UClass_AWater_NoRegister()
	{
		return AWater::StaticClass();
	}
	struct Z_Construct_UClass_AWater_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ok_MetaData[];
#endif
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_ok;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_AWater_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AActor,
		(UObject* (*)())Z_Construct_UPackage__Script_REALunrealcpp,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AWater_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "Water.h" },
		{ "ModuleRelativePath", "Water.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AWater_Statics::NewProp_ok_MetaData[] = {
		{ "Category", "Water" },
		{ "ModuleRelativePath", "Water.h" },
	};
#endif
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UClass_AWater_Statics::NewProp_ok = { "ok", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AWater, ok), METADATA_PARAMS(Z_Construct_UClass_AWater_Statics::NewProp_ok_MetaData, ARRAY_COUNT(Z_Construct_UClass_AWater_Statics::NewProp_ok_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_AWater_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AWater_Statics::NewProp_ok,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_AWater_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<AWater>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_AWater_Statics::ClassParams = {
		&AWater::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_AWater_Statics::PropPointers,
		nullptr,
		ARRAY_COUNT(DependentSingletons),
		0,
		ARRAY_COUNT(Z_Construct_UClass_AWater_Statics::PropPointers),
		0,
		0x009000A0u,
		METADATA_PARAMS(Z_Construct_UClass_AWater_Statics::Class_MetaDataParams, ARRAY_COUNT(Z_Construct_UClass_AWater_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_AWater()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_AWater_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(AWater, 3687973984);
	template<> REALUNREALCPP_API UClass* StaticClass<AWater>()
	{
		return AWater::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_AWater(Z_Construct_UClass_AWater, &AWater::StaticClass, TEXT("/Script/REALunrealcpp"), TEXT("AWater"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(AWater);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
