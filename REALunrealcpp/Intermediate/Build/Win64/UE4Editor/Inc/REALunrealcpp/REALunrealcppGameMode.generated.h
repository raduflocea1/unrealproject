// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef REALUNREALCPP_REALunrealcppGameMode_generated_h
#error "REALunrealcppGameMode.generated.h already included, missing '#pragma once' in REALunrealcppGameMode.h"
#endif
#define REALUNREALCPP_REALunrealcppGameMode_generated_h

#define REALunrealcpp_Source_REALunrealcpp_REALunrealcppGameMode_h_12_RPC_WRAPPERS
#define REALunrealcpp_Source_REALunrealcpp_REALunrealcppGameMode_h_12_RPC_WRAPPERS_NO_PURE_DECLS
#define REALunrealcpp_Source_REALunrealcpp_REALunrealcppGameMode_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAREALunrealcppGameMode(); \
	friend struct Z_Construct_UClass_AREALunrealcppGameMode_Statics; \
public: \
	DECLARE_CLASS(AREALunrealcppGameMode, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/REALunrealcpp"), REALUNREALCPP_API) \
	DECLARE_SERIALIZER(AREALunrealcppGameMode)


#define REALunrealcpp_Source_REALunrealcpp_REALunrealcppGameMode_h_12_INCLASS \
private: \
	static void StaticRegisterNativesAREALunrealcppGameMode(); \
	friend struct Z_Construct_UClass_AREALunrealcppGameMode_Statics; \
public: \
	DECLARE_CLASS(AREALunrealcppGameMode, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/REALunrealcpp"), REALUNREALCPP_API) \
	DECLARE_SERIALIZER(AREALunrealcppGameMode)


#define REALunrealcpp_Source_REALunrealcpp_REALunrealcppGameMode_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	REALUNREALCPP_API AREALunrealcppGameMode(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AREALunrealcppGameMode) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(REALUNREALCPP_API, AREALunrealcppGameMode); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AREALunrealcppGameMode); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	REALUNREALCPP_API AREALunrealcppGameMode(AREALunrealcppGameMode&&); \
	REALUNREALCPP_API AREALunrealcppGameMode(const AREALunrealcppGameMode&); \
public:


#define REALunrealcpp_Source_REALunrealcpp_REALunrealcppGameMode_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	REALUNREALCPP_API AREALunrealcppGameMode(AREALunrealcppGameMode&&); \
	REALUNREALCPP_API AREALunrealcppGameMode(const AREALunrealcppGameMode&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(REALUNREALCPP_API, AREALunrealcppGameMode); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AREALunrealcppGameMode); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AREALunrealcppGameMode)


#define REALunrealcpp_Source_REALunrealcpp_REALunrealcppGameMode_h_12_PRIVATE_PROPERTY_OFFSET
#define REALunrealcpp_Source_REALunrealcpp_REALunrealcppGameMode_h_9_PROLOG
#define REALunrealcpp_Source_REALunrealcpp_REALunrealcppGameMode_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	REALunrealcpp_Source_REALunrealcpp_REALunrealcppGameMode_h_12_PRIVATE_PROPERTY_OFFSET \
	REALunrealcpp_Source_REALunrealcpp_REALunrealcppGameMode_h_12_RPC_WRAPPERS \
	REALunrealcpp_Source_REALunrealcpp_REALunrealcppGameMode_h_12_INCLASS \
	REALunrealcpp_Source_REALunrealcpp_REALunrealcppGameMode_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define REALunrealcpp_Source_REALunrealcpp_REALunrealcppGameMode_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	REALunrealcpp_Source_REALunrealcpp_REALunrealcppGameMode_h_12_PRIVATE_PROPERTY_OFFSET \
	REALunrealcpp_Source_REALunrealcpp_REALunrealcppGameMode_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	REALunrealcpp_Source_REALunrealcpp_REALunrealcppGameMode_h_12_INCLASS_NO_PURE_DECLS \
	REALunrealcpp_Source_REALunrealcpp_REALunrealcppGameMode_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> REALUNREALCPP_API UClass* StaticClass<class AREALunrealcppGameMode>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID REALunrealcpp_Source_REALunrealcpp_REALunrealcppGameMode_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
