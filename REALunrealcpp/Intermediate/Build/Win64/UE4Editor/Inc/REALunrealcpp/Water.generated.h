// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef REALUNREALCPP_Water_generated_h
#error "Water.generated.h already included, missing '#pragma once' in Water.h"
#endif
#define REALUNREALCPP_Water_generated_h

#define REALunrealcpp_Source_REALunrealcpp_Water_h_12_RPC_WRAPPERS
#define REALunrealcpp_Source_REALunrealcpp_Water_h_12_RPC_WRAPPERS_NO_PURE_DECLS
#define REALunrealcpp_Source_REALunrealcpp_Water_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAWater(); \
	friend struct Z_Construct_UClass_AWater_Statics; \
public: \
	DECLARE_CLASS(AWater, AActor, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/REALunrealcpp"), NO_API) \
	DECLARE_SERIALIZER(AWater)


#define REALunrealcpp_Source_REALunrealcpp_Water_h_12_INCLASS \
private: \
	static void StaticRegisterNativesAWater(); \
	friend struct Z_Construct_UClass_AWater_Statics; \
public: \
	DECLARE_CLASS(AWater, AActor, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/REALunrealcpp"), NO_API) \
	DECLARE_SERIALIZER(AWater)


#define REALunrealcpp_Source_REALunrealcpp_Water_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AWater(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AWater) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AWater); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AWater); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AWater(AWater&&); \
	NO_API AWater(const AWater&); \
public:


#define REALunrealcpp_Source_REALunrealcpp_Water_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AWater(AWater&&); \
	NO_API AWater(const AWater&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AWater); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AWater); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AWater)


#define REALunrealcpp_Source_REALunrealcpp_Water_h_12_PRIVATE_PROPERTY_OFFSET
#define REALunrealcpp_Source_REALunrealcpp_Water_h_9_PROLOG
#define REALunrealcpp_Source_REALunrealcpp_Water_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	REALunrealcpp_Source_REALunrealcpp_Water_h_12_PRIVATE_PROPERTY_OFFSET \
	REALunrealcpp_Source_REALunrealcpp_Water_h_12_RPC_WRAPPERS \
	REALunrealcpp_Source_REALunrealcpp_Water_h_12_INCLASS \
	REALunrealcpp_Source_REALunrealcpp_Water_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define REALunrealcpp_Source_REALunrealcpp_Water_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	REALunrealcpp_Source_REALunrealcpp_Water_h_12_PRIVATE_PROPERTY_OFFSET \
	REALunrealcpp_Source_REALunrealcpp_Water_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	REALunrealcpp_Source_REALunrealcpp_Water_h_12_INCLASS_NO_PURE_DECLS \
	REALunrealcpp_Source_REALunrealcpp_Water_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> REALUNREALCPP_API UClass* StaticClass<class AWater>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID REALunrealcpp_Source_REALunrealcpp_Water_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
